import React from 'react'
import { Text, View, Button,TextInput, StyleSheet, FlatList } from 'react-native'
import FilmsItem from './FilmItem';
import films from '../Helpers/filmsData'

class Search extends React.Component{

render(){   
        return(
           <View style={styles.container}>
                <TextInput style={styles.textInput} placeholder="Titre du film"/>
               <Button style={styles.button} title="Search" onPress={()=>{alert('Searching .....')}}/>
               <FlatList
               data={films}
               keyExtractor={(item) =>item.title.toString()}
              renderItem={({item}) => <FilmsItem film={item}/>}/>
            
                   
           </View>
          
        )

        
    }
}
const styles= StyleSheet.create({
    container:{
       flex:1,
       margin:10
    },
    textInput:{
       margin:10,
       height:40,
       borderColor:'skyblue',
       borderWidth:2
     },
     button:{
         marginLeft:30,
         marginRight:30,
         borderRadius:5
     }
     
  
});

export default Search